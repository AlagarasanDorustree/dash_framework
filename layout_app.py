# Run this app with `python layout_app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd

app = Dash(__name__)

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

colors = {
    'background': 'grey',
    'text': 'black',
    'plot': 'white'
}

df = pd.DataFrame({
   "Category": ["Cost", "Power", "Top Speed", "Cost", "Power", "Top Speed"],
   "Score": [1, 5, 5, 5, 3, 4],
   "Bike": ["H2R", "H2R", "H2R", "Hayabusa", "Hayabusa", "Hayabusa"]
})

fig = px.bar(df, x="Category", y="Score", color="Bike", barmode="group", color_discrete_map={"H2R": 'green', "Hayabusa": 'blue'})

fig.update_layout(
    plot_bgcolor=colors['plot'],
    paper_bgcolor=colors['background'],
    font_color=colors['text']
)


app.layout = html.Div(children=[
    html.H1(children='Test Dash',
            style={
                'textAlign': 'center',
                'color': colors['text']
            }),

    html.Div(children='''A sample web application for visualising given data.''',
             style={
                 'textAlign': 'center',
                 'color': colors['text']
             }),

    dcc.Graph(
        id='example-graph',
        figure=fig
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
