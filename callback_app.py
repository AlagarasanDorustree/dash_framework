from dash import Dash, html, dcc, Input, Output
import plotly.express as px
import pandas as pd

app = Dash(__name__)

df = pd.read_csv("climate.csv")
avg_df = df.groupby(pd.PeriodIndex(df['Date'], freq="y"))['MINT (C)', 'MAXT (C)', 'RH1 (%)', 'WS (MS)'].mean().reset_index()

app.layout = html.Div(children=[

    html.H1(children='''A sample responsive web application.''',
            style={
                'textAlign': 'center',
                'color': 'black'
            }),
    html.Br(),

    dcc.Graph(
        id='example-graph',
    ),
    dcc.Slider(
        avg_df['Date'].min().year,
        avg_df['Date'].max().year,
        step=None,marks={str(year): str(year) for year in avg_df['Date'].unique()},
        value=avg_df['Date'].min().year,
        id='slider')

])

@app.callback(
    Output('example-graph', 'figure'),
    Input('slider', 'value')
)
def update_output_div(input_value):
    filtered_df = avg_df.loc[avg_df['Date'] <= str(input_value)]
    fig = px.scatter(filtered_df, x="MINT (C)", y="MAXT (C)",
                     size="RH1 (%)", color="WS (MS)")

    return fig


if __name__ == '__main__':
    app.run_server(debug=True)
