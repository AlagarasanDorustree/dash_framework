# dash_framework

Dash framework in python is for creating interactive analytical web applications. It uses Flask for the backend and React.js components for the frontend.
Dash was developed by plotly in 2017.Plotly is a company based on technical computing that provides online graphing, analytics, and statistics tools.



## To run the application in local machine:
### Requirements
* Python 3.8
* Pipenv

```bash
$ cd to  [project root directory]
$ apt install python3.8-venv
$ python3 -m venv <virtualenv name> 
$ source <virtualenv name>/bin/activate
$ pip install -r requirements.txt
$ python callback_app.py/layout_app.py

```
